Source: ruby-sanitize
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Jonas Genannt <jonas.genannt@capi2name.de>
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               rake,
               ruby-crass (>= 1.0.2),
               ruby-minitest,
               ruby-nokogiri (>= 1.16.8)
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-sanitize.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-sanitize
Homepage: https://github.com/rgrove/sanitize/
Testsuite: autopkgtest-pkg-ruby
Rules-Requires-Root: no

Package: ruby-sanitize
Architecture: all
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends}
Description: whitelist-based HTML sanitizer
 Sanitize is a whitelist-based HTML sanitizer. Given a list of acceptable
 elements and attributes, Sanitize will remove all unacceptable HTML from a
 string.
 .
 Using a simple configuration syntax, you can tell Sanitize to allow certain
 elements, certain attributes within those elements, and even certain URL
 protocols within attributes that contain URLs. Any HTML elements or attributes
 that you don't explicitly allow will be removed.
 .
 Because it's based on Nokogiri, a full-fledged HTML parser, rather than a bunch
 of fragile regular expressions, Sanitize has no trouble dealing with malformed
 or maliciously-formed HTML and returning safe output.
